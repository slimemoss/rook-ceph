# Rook Ceph
テスト・デプロイは手動です。

変更は多くないし、インフラに近くて仮想化できないからテストしにくいので。

## クイックスタート
```
kubectl apply -f crds.yaml -f common.yaml -f operator.yaml
kubectl apply -f cluster.yaml
kubectl apply -f storageclass.yaml
```

## Ingress
```
kubectl apply -f ingress.yaml
```
