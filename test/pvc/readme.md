# pvcのテスト

## 手順
### deployment, pvcの作成
```
kubectl apply -f pvc_test.yaml
```

### pvc内のファイル編集
```
kubectl -n pvc-test exec -it pvc-test -- touch pvc/1
```

### podの削除
```
kubectl -n pvc-test delete pod pvc-test
```

### pvc内のファイル確認
```
kubectl -n pvc-test exec -it pvc-test -- touch pvc/1
```
